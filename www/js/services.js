angular.module('ionicApp.services', [])
.factory("Groups", function($firebaseArray) {
	var self = this;
	self.allGroups = function() {  
  	var groupsRef = new Firebase("https://studentManagement.firebaseio.com/groups");
  	return $firebaseArray(groupsRef);
  }
  return self;
})
.factory("Members", function($firebaseArray){
	var self = this;
	self.allMembersGroup = function(groupId) {
		var membersRef = new Firebase("https://studentManagement.firebaseio.com/groups/" + groupId + "/members");
		// var membersgroupRef = membersRef.child('membergroupId/'+currGroup.$id);
		console.log(groupId);
		return $firebaseArray(membersRef);
	}  
	return self;
});