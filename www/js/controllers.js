angular.module('ionicApp.controllers', [])

.controller('UserCtrl', function($scope, $state) {
  $scope.signIn = function(user) {
    console.log('Sign-In', user);
    $state.go('eventmenu.home');
  };
  
})
.controller('MainCtrl', function($scope, $ionicSideMenuDelegate, $state, $ionicModal, Groups, Members, $ionicListDelegate, $firebaseArray) {
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.viewAccount = function(userId) {
    console.log('View account')
    $state.go('eventmenu.account');
    $scope.state = 'account';
  }

   /***************************** Functions Reletaed to Groups Management start************************/
  $scope.groups = [];

  $scope.updateGroupList = function() {

     $scope.groups = Groups.allGroups();
  }
  $scope.updateGroupList();
  var addGroupPopup = function() {
    $ionicModal.fromTemplateUrl('templates/add-group.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
    .then(function(modal) {
      $scope.group = [];
      $scope.modaladdgroup = modal;
    });
  }
  addGroupPopup();
  $scope.opengroupModal = function() {
    $scope.groupediting = false;
    if ($scope.modaladdgroup) $scope.modaladdgroup.show();
  };
  $scope.closegroupModal = function() {
    $scope.modaladdgroup.hide();
    setTimeout(function() {
      $scope.modaladdgroup.remove();
      addGroupPopup();
    }, 500);
  };
  $scope.saveGroup = function(group) {
         if (group !== undefined && group.editgroupID !== undefined && group.editgroupID !== '') {
            var itemRef = new Firebase("https://studentManagement.firebaseio.com/groups" + '/' + group.editgroupID);
            itemRef.update({name: group.name});
            $scope.closegroupModal();
         }
         else if (group !== undefined && group.name !== undefined && group.name !== '') {
            $scope.groups.$add({
               "name": group.name
            });
            $scope.closegroupModal();
         }
      };
      $scope.deleteGroup = function(groupId) {
        var itemRef = new Firebase("https://studentManagement.firebaseio.com/groups" + '/' + groupId);
        itemRef.remove();
      };
      $scope.editGroup = function(group) {
         if (group !== undefined && group.$id !== undefined && group.$id !== '') {
            $scope.groupediting = true;
            $scope.modaladdgroup.show();  
            $scope.group.name = group.name;
            $scope.group.editgroupID = group.$id;
         }
      };
  $scope.viewGroupMemberInfor = function(group) {
    console.log('View group member infor', group);
    $scope.currentGroup = group;
    $scope.updateGroupList();
    $state.go('eventmenu.groupmember');
    $scope.state = 'groupmember';
  }
    /***************************** Functions Reletaed to Groups Management end************************/

    /***************************** Functions Reletaed to Members Management start************************/ 
  $scope.membersGroup = [];
  $scope.updateGroupList = function() {
     $scope.membersGroup = Members.allMembersGroup($scope.currentGroup.$id);
     console.log($scope.currentGroup.name);
  }
  var addMemberGroupPopup = function() {
    $ionicModal.fromTemplateUrl('templates/add-member.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
    .then(function(modal) {
      $scope.memberGroup = [];
      $scope.modaladdmembergroup = modal;
    });
  }
  addMemberGroupPopup();
  $scope.openmembergroupModal = function() {
    $scope.membergroupediting = false;
    if ($scope.modaladdmembergroup) $scope.modaladdmembergroup.show();
  };
  $scope.closemembergroupModal = function() {
    $scope.modaladdmembergroup.hide();
    setTimeout(function() {
      $scope.modaladdmembergroup.remove();
      addMemberGroupPopup();
    }, 500);
  };
  $scope.saveMemberGroup = function(memberGroup) {
         if (memberGroup !== undefined && memberGroup.editmembergroupID !== undefined && memberGroup.editmembergroupID !== '') {
            var itemRef = new Firebase("https://studentManagement.firebaseio.com/groups/" + $scope.currentGroup.$id + memberGroup.editmembergroupID);
            itemRef.update({membername: memberGroup.membername});
            itemRef.update({memberemail: memberGroup.memberemail});
            itemRef.update({memberphonenumber: memberGroup.memberphonenumber});
            $scope.closemembergroupModal();
         }
         else if (memberGroup !== undefined && memberGroup.membername !== undefined && memberGroup.membername !== '') {
            $scope.membersGroup.$add({
               "membername": memberGroup.membername,
               "memberemail": memberGroup.memberemail,
               "memberphonenumber": memberGroup.memberphonenumber,
               // "membergroupId": $scope.currentGroup.$id
            });
            $scope.closemembergroupModal();
         }
      };
      $scope.deleteMemberGroup = function(membergroupId) {
        var itemRef = new Firebase("https://studentManagement.firebaseio.com/groups/" + $scope.currentGroup.$id + membergroupId);
        itemRef.remove();
      };
      $scope.editMemberGroup = function(memberGroup) {
         if (memberGroup !== undefined && memberGroup.$id !== undefined && memberGroup.$id !== '') {
            $scope.membergroupediting = true;
            $scope.modaladdmembergroup.show();  
            $scope.memberGroup.membername = memberGroup.membername;
            $scope.memberGroup.memberemail = memberGroup.memberemail;
            $scope.memberGroup.memberphonenumber = memberGroup.memberphonenumber;
            // $scope.memberGroup.editmembergroupID = memberGroup.$id;
         }
      };
    /***************************** Functions Reletaed to Members Management end************************/
});
