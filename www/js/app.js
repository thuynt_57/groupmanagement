angular.module('ionicApp', ['ionic', 'ionicApp.controllers', 'ionicApp.services', 'firebase'])
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('signin', {
    url: '/sign-in',
    templateUrl: 'templates/sign-in.html',
    controller: 'UserCtrl'
  })
  .state('forgotpassword', {
    url: '/forgot-password',
    templateUrl: 'templates/forgot-password.html'
  })
  .state('eventmenu', {
      url: "/event",
      abstract: true,
      templateUrl: "templates/event-menu.html",
      controller: "MainCtrl"
    })
  .state('eventmenu.home', {
    url: "/home",
    views: {
      'menuContent' :{
        templateUrl: "templates/home.html"
      }
    },
    parent: 'eventmenu'
  })
  .state('eventmenu.groupmember', {
      url: '/group-member',
      views: {
        'menuContent' :{
          templateUrl: 'templates/group-member.html'
        }
      },
      parent: 'eventmenu'
  })
    .state('eventmenu.account', {
    url: "/account",
    views: {
      'menuContent' :{
        templateUrl: "templates/account.html"      }
    }
  })
  .state('addgroup',{
    url: "/addgroup",
    templateUrl: "templates/add-group.html",
    controller: "MainCtrl"
  })

  .state('eventmenu.groupmember.edit', {
    url: "/edit",
    views: {
      'menuContent' :{
        templateUrl: "templates/edit-member-infor.html",
        controller: "MainCtrl"
      }
    }
  })

  $urlRouterProvider.otherwise('/sign-in');

});
